<?php

declare(strict_types=1);

namespace Ucc\Exceptions;

use Exception;

/**
 * We use this class just for demo purpose. In real project, we should add more information.
 *
 * Class UnknownException
 * @package Ucc\Exceptions
 */
class UnknownException extends Exception
{
    public function __construct(string $message)
    {
        parent::__construct($message, 500);
    }
}
