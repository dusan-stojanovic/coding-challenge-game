<?php

declare(strict_types=1);

namespace Ucc\Repositories;

use JsonMapper;
use JsonMapper_Exception;
use KHerGe\JSON\Exception\DecodeException;
use KHerGe\JSON\Exception\UnknownException;
use KHerGe\JSON\JSON;
use Ucc\Exceptions\UnknownException as UccUnknownException;
use Ucc\Models\Question;

/**
 * Class QuestionRepository
 * @package Ucc\Repositories
 */
class QuestionRepository
{
    private const QUESTIONS_PATH = __DIR__ . '/../../questions.json';

    private JSON $json;
    private JsonMapper $jsonMapper;

    public function __construct(JSON $json, JsonMapper $jsonMapper)
    {
        $this->json = $json;
        $this->jsonMapper = $jsonMapper;
    }

    /**
     * @return Question[]
     * @throws UccUnknownException
     */
    public function getAll(): array
    {
        try {
            $questionsData = $this->json->decodeFile(self::QUESTIONS_PATH);
        } catch (DecodeException $e) {
            throw new UccUnknownException($e->getMessage());
        } catch (UnknownException $e) {
            throw new UccUnknownException($e->getMessage());
        }
        try {
            /** @var Question[] $questions */
            $questions = $this->jsonMapper->mapArray($questionsData, [], Question::class);
        } catch (JsonMapper_Exception $e) {
            throw new UccUnknownException($e->getMessage());
        }

        $allQuestions = [];
        foreach ($questions as $question) {
            $allQuestions[$question->getId()] = $question;
        }

        return $allQuestions;
    }

    /**
     * @param int $id
     * @return Question
     * @throws UccUnknownException
     */
    public function getById(int $id): Question
    {
        $allQuestion = $this->getAll();

        return $allQuestion[$id];
    }
}
