<?php

namespace Ucc\Controllers;

use Ucc\Exceptions\UnknownException;
use Ucc\Http\JsonResponseTrait;
use Ucc\Models\Question;
use Ucc\Services\QuestionService;
use Ucc\Session;

class QuestionsController extends Controller
{
    use JsonResponseTrait;

    private QuestionService $questionService;

    public function __construct(QuestionService $questionService)
    {
        parent::__construct();
        $this->questionService = $questionService;
    }

    public function beginGame(): void
    {
        $name = $this->requestBody->name ?? null;
        if (empty($name)) {
            $response = $this->json(['error' => 'You must provide a name'], 400);
            if ($response) {
                echo $this->jsonBody;
            }
            return;
        }

        Session::set('name', $name);
        Session::set('questionCount', 1);

        try {
            /** @var Question[] $randomQuestions */
            $randomQuestions = $this->questionService->getRandomQuestions();
        } catch (UnknownException $e) {
            $response = $this->json(['error' => $e->getMessage()], $e->getCode());
            if ($response) {
                echo $this->jsonBody;
            }
            return;
        }

        Session::set('questions', serialize($randomQuestions));

        $question = $this->getQuestion();

        $response = $this->json(['question' => $question], 201);
        if ($response) {
            echo $this->jsonBody;
        }
    }

    public function answerQuestion(int $id): void
    {
        if ( Session::get('name') === null ) {
            $response = $this->json(['error' => 'You must first begin a game'], 400);
            if ($response) {
                echo $this->jsonBody;
            }
            return;
        }

        if ((int)Session::get('questionCount') > 4) {
            $name = Session::get('name');
            $points = Session::get('points');
            Session::destroy();
            $response = $this->json(['message' => "Thank you for playing {$name}. Your total score was: {$points} points!"]);
            if ($response) {
                echo $this->jsonBody;
            }
            return;
        }

        $answer = $this->requestBody->answer ?? null;
        if (empty($answer)) {
            $response =  $this->json(['error' => 'You must provide an answer'], 400);
            if ($response) {
                echo $this->jsonBody;
            }
            return;
        }

        try {
            $points = $this->questionService->getPointsForAnswer($id, $answer);
        } catch (UnknownException $e) {
            $response = $this->json(['error' => $e->getMessage()], $e->getCode());
            if ($response) {
                echo $this->jsonBody;
            }
            return;
        }

        $message = $points === 0 ? 'Answer is incorrect' : 'Answer is correct';
        $this->updatePoints($points);
        $question = $this->getNextQuestion();

        $response = $this->json(['message' => $message, 'question' => $question]);
        if ($response) {
            echo $this->jsonBody;
        }
    }

    private function getQuestion(): Question
    {
        $questionCount = Session::get('questionCount');
        $questions = unserialize(Session::get('questions'), ['allowed_classes' => [Question::class]]);

        return $questions[$questionCount -1];
    }

    /**
     * @return Question
     */
    private function getNextQuestion(): Question
    {
        $questionCount = (int) Session::get('questionCount');
        Session::set('questionCount', ++$questionCount);

        return $this->getQuestion();
    }

    /**
     * @param int $points
     */
    private function updatePoints(int $points): void
    {
        $currentPoints = (int) Session::get('points');

        Session::set('points', $currentPoints + $points);
    }
}
