<?php


namespace Ucc\Http;


use JsonException;

trait JsonResponseTrait
{
    protected string $jsonBody;

    public function json($data, int $statusCode = 200): bool
    {
        http_response_code($statusCode);
        header('Session-Id: ' . session_id());
        header('Content-Type: application/json;charset=utf-8');
        try {
            $this->jsonBody = json_encode($data, JSON_THROW_ON_ERROR);
        } catch (JsonException $e) {
            return false;
        }

        return json_last_error() === JSON_ERROR_NONE;
    }
}
