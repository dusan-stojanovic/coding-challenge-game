<?php

namespace Ucc\Services;

use Ucc\Exceptions\UnknownException as UccUnknownException;
use Ucc\Repositories\QuestionRepository;

class QuestionService
{
    private QuestionRepository $questionRepository;

    public function __construct(QuestionRepository $questionRepository)
    {
        $this->questionRepository = $questionRepository;
    }

    /**
     * @param int $count
     * @return array
     * @throws UccUnknownException
     */
    public function getRandomQuestions(int $count = 5): array
    {
        $allQuestions = $this->questionRepository->getAll();
        shuffle($allQuestions);

        return array_slice($allQuestions, 0, $count);
    }

    /**
     * @param int $id
     * @param string $answer
     * @return int
     * @throws UccUnknownException
     */
    public function getPointsForAnswer(int $id, string $answer): int
    {
        $question = $this->questionRepository->getById($id);

        $isCorrectAnswer = $question->getCorrectAnswer() === $answer;

        return $isCorrectAnswer ? $question->getPoints() : 0;
    }
}
